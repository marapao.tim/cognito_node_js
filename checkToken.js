//app.js
"use strict";
 
const  CognitoExpress = require("cognito-express"),
    port = process.env.PORT || 8000; 
 
//Initializing CognitoExpress constructor
const cognitoExpress = new CognitoExpress({
    region: "us-east-1",
    cognitoUserPoolId: "us-east-1_1I4VKHwIw",
    tokenUse: "access", //Possible Values: access | id
    tokenExpiration: 3600000 //Up to default expiration of 1 hour (3600000 ms)
});
 
//Our middleware that authenticates all APIs under our 'authenticatedRoute' Router
cognitoExpress.validate('eyJraWQiOiJcL0kzWEM0bmc3YnI1dFljMG9EbzlqR0lLYU5peHQ4WW9VRFwvV253UytKRlU9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI0NTllMjk3Yi0wNDM5LTQ0N2ItODcwYy0wZWUyOGE2MGZkY2UiLCJjb2duaXRvOmdyb3VwcyI6WyJhZG1pbiJdLCJldmVudF9pZCI6ImI0ZWQzNmUwLTVhOTItMTFlOC1hMTFmLTE1OTg1YzgyYzNiYyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1MjY2NDQ2NzgsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xXzFJNFZLSHdJdyIsImV4cCI6MTUyNjY0ODI3OCwiaWF0IjoxNTI2NjQ0Njc4LCJqdGkiOiI4YmE3Y2E3Yy03YTA1LTQ2MDQtODE2OC02NjQ5NWVlMTMzYmYiLCJjbGllbnRfaWQiOiI1NDJia2VoZmtzaWdjbzJqbjQ4YTlkdWpodSIsInVzZXJuYW1lIjoiaGVsbG93b3JsZCJ9.Q9ht9ILlrZiOiigQKSceRt_ZP-TVAonA27Ue6TNoqOY1krzO3jrpFI9lvEzwjPsUWzlyQTBUYLkYeSJW30VZdPvwLFEh3LjXGcRtZRc-UZq6aQyFxfamEklTRfCklxSwQZehzatcHNKGVDjWjsqx71iI6fcGVfOoKix_9q1GrKMRTJVvqyTM-i_JaiMdYfCXhU6S2HoBKA4TT3qGZ9i95e4yWooc1PlX4n6ZO1RoNbBkgYLRty_B4vdIwiuj9272IKFzD8S_QWsBp4IW4rRCLFaOSXlsUGoi6JOkh5xY5lbQqcIBk7J5xO1G_OU9ugOTA3V4IYr6-yvhj5SJjsYd2A', function(err, response) {
    if (err) {
        console.log('error ' + err);
        /*
            //API is not authenticated, do something with the error.
            //Perhaps redirect user back to the login page
            
            //ERROR TYPES:
            
            //If accessTokenFromClient is null or undefined
            err = {
                "name": "TokenNotFound",
                "message": "access token not found"
            }
            
            //If tokenuse doesn't match accessTokenFromClient
            {
                "name": "InvalidTokenUse",
                "message": "Not an id token"
            }
 
            //If token expired
            err = {
                "name": "TokenExpiredError",
                "message": "jwt expired",
                "expiredAt": "2017-07-05T16:41:59.000Z"
            }
 
            //If token's user pool doesn't match the one defined in constructor
            {
                "name": "InvalidUserPool",
                "message": "access token is not from the defined user pool"
            }
 
        */
    } else {
        //Else API has been authenticated. Proceed.
        //res.locals.user = response; //Optional - if you want to capture user information
     console.log(response);
       // next();
    }
});